<?php
require ('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new Animal("shaun",2, "false");
    
    echo $sheep->name. "<br>"; // "shaun"
    echo $sheep->legs. "<br>"; // 2
    echo $sheep->cold_blooded. "<br>"; // false

    $sungokong = new Ape("kera sakti",2, "");
    echo $sungokong->name. "<br>";
    echo $sungokong->legs . "<br>";
    $sungokong->yell() ;// "Auooo"

    $kodok = new Frog("buduk",4);
    echo $kodok->name. "<br>";
    echo $kodok->legs. "<br>";
$kodok->jump() ; // "hop hop"

?>